#!/bin/bash

desthome="./episodes/"

function getFile() {
    src=$1
    dest=$2
    wget "$src" -O "$dest"
}

function setMetadata() {
    episode=$1
    artist=$2
    album=$3
    title=$4
    airdate=$5
    teaser=$6
    image=$7

    #mid3v2 -v -a "\"$artist\"" -A "\"$album\"" -t "\"$title\"" -y $airdate -c "\"$teaser\"" -p "\"$image\"" 
    mid3v2 -v -a "$artist" -A "$album" -t "$title" -y $airdate -c "$teaser" -p "$image" "$episode"
}

while IFS='\n' read -r line || [[ -n "$line" ]]; do
    srcUrl=$(echo "$line" | cut -f1)
    destFile=$(echo "$line" | cut -f2)
    artist=$(echo "$line" | cut -f3)
    album=$(echo "$line" |cut -f4)
    title=$(echo "$line" |cut -f5)
    airdate=$(echo "$line" | cut -f6)
    teaser=$(echo "$line" | cut -f7)
    imgsrc=$(echo "$line" | cut -f8)
    imgFile=$(echo "$line" |cut -f9)
    
    #printf "Source: %s\n" "$srcUrl"
    printf "File: %s\n" "$destFile"
    #printf "Artist: %s\n" "$artist"
    #printf "Album: %s\n" "$album"
    #printf "Airdate: %s\n" "$airdate"
    #printf "Teaser: %s\n" "$teaser"
    #printf "ImgSrc: %s\n" "$imgsrc"
    #printf "Img File: %s\n" "$imgFile"
    #printf "\n"
    
    getFile "$srcUrl" "$desthome$destFile"
    getFile "$imgsrc" "$desthome$imgFile"

    setMetadata "$desthome$destFile" "$artist" "$album" "$title" "$airdate" "$teaser" "$desthome$imgFile"
done < "$1"
