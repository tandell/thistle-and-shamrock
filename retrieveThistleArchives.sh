#!/bin/bash

#
# Dependencies required:
#	* wget
#	* xml_grep
#	* package: perl-HTML-Tidy
#	* xmlstarlet
#	* jq

url="http://www.npr.org/series/103063413/the-thistle-and-shamrock/archive?date="

archives=()

echo "Retrieving monthly archives of The Thistle and Shamrock"
for year in $(seq 2016 2017)
do
	for month in $(seq 1 12)
	do
		src_url="$url$month-01-$year"

		echo "Retrieving archive $month-$year"
		wget_output=$(wget --tries=1 -q $src_url -O archives/thistle-$year-$month)
		if [ $? -ne 0 ]; then
			echo "Error retrieving: $src_url"
		fi
	done
done

