# The Thistle and Shamrock Archive Archiver

This project will download the existing archives from NPR of the show Thistle and Shamrock to allow for local playback


archives  data-archives  downloadEpisodes.sh  episodes  links  parseThistleArchives.sh  queue.data  retrieveThistleArchives.sh  thistle.sh  Thistle & Shamrock  tidyArchive.tmp

## Setup

1. Download scripts
2. Create the directories:
  * `archives`
  * `data-archives`
  * `episodes`
3. Modify `retrieveThistleArchives.sh` to grab the date range required

## Running

1. Execute `retrieveThistleArchives.sh`
2. Execute `parseThistleArchives.sh`
3. Execute `downloadEpisodes.sh queue.data`

All requested episodes should be downloaded and renamed into the `episodes` directory.

