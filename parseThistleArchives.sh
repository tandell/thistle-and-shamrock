#!/bin/bash

#
# Dependencies required:
#	* wget
#	* xml_grep
#	* package: perl-HTML-Tidy
#	* xmlstarlet
#	* jq

function writeEpisode() {
    sourceUrl=$1
    showdata=$2
    albumArt=$3

    if [ -z $albumArt ]; then
	albumArt="http://media.npr.org/assets/music/programs/thistle/thistle_wide-df6f1c23f31bb47d68724ef3937377aa0b27d57f-s300-c85.jpg"
    fi

    artist="Fiona Ritchie"
    album="The Thistle & Shamrock"
    song=$(echo "$showdata" | xmlstarlet sel -T -t -v "//h2[@class='title']/a/text()" | sed -e 's/^.*: //g')
    teaser=$(echo "$showdata" | xmlstarlet sel -T -t -v "//p[@class='teaser']/a/text()" |tr -d '\n')
    airdate=$(echo "$showdata" | xmlstarlet sel -T -t -v "//p[@class='teaser']/a/time/@datetime" )

    filename="Thistle & Shamrock - $airdate - $song"
    songfile="$filename.mp3"
    imgfile="$filename.jpg"

    printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "$sourceUrl" "$songfile" "$artist" "$album" "$song" "$airdate" "$teaser" "$albumArt" "$imgfile" >> $queue
}

function writeMissingEpisode() {
    sourceUrl=$1
    
    printf "%s\n" "$sourceUrl" >> $missing
}

function noImageItems() {
    data=$1
    list=$2

    for item in $(seq 1 $list )
    do
		showrawdata=$(xmlstarlet sel -t -c "(//article[@class='item no-image'])[${item}]" tidyArchive.tmp )
		showdata=$(echo "$showrawdata" | xmlstarlet sel -T -t -v "//@data-audio|//div[@class='item-info']/h2/a/@href" 2> /dev/null | tr '\n' '\t')
		showloc=$(echo "$showdata" | cut -f1)
		dataAudio=$(echo "$showdata" | cut -f2)
		
		# An odd thing about cut is that it will duplicate the input if no fields are detected.
		if [ "$showloc" = "$dataAudio" ]; then
		    unset dataAudio
		fi

		rawAudioUrl=$(echo "$dataAudio"|jq -r .audioUrl)
	    if [[ $rawAudioUrl =~ ^http.* ]]; then
		# Strip any extranous characters after the URL.
		hashedAudioUrl=$(echo "${rawAudioUrl%%\?*}")
	    else
	        hashedAudioUrl=$(echo "$rawAudioUrl"|base64 -d|cut -d'?' -f1)
	    fi

		if [ -n "$hashedAudioUrl" ]; then
		    writeEpisode "$hashedAudioUrl" "$showrawdata"
		fi
		if [ -z "$hashedAudioUrl" ]; then
		    writeMissingEpisode "$showloc"
		fi
    done
}

function imageItems() {
    data=$1
    list=$2

    for item in $(seq 1 $items)
    do
		showrawdata=$(xmlstarlet sel -t -c "(//article[@class='item has-image'])[${item}]" tidyArchive.tmp )

		showdata=$(echo "$showrawdata" | xmlstarlet sel -T -t -v "//@data-audio|//div[@class='item-info']/h2/a/@href" 2> /dev/null | tr '\n' '\t')
		showloc=$(echo "$showdata" | cut -f1)
		imageloc=$(echo "$showrawdata" | xmlstarlet sel -T -t -v "//img/@data-original" 2> /dev/null)
		# echo "${imageloc}"
		dataAudio=$(echo "$showdata" | cut -f2)
		# An odd thing about cut is that it will duplicate the input if no fields are detected.
		if [ "$showloc" = "$dataAudio" ]; then
		    unset dataAudio
		fi

		rawAudioUrl=$(echo $dataAudio|jq -r '.audioUrl'|base64 -d|cut -d'?' -f1)
		if [ -n "$rawAudioUrl" ]; then
		    writeEpisode "$rawAudioUrl" "$showrawdata" "$imageloc"
		fi
		if [ -z "$rawAudioUrl" ]; then
		    writeMissingEpisode "$showloc"
		fi
    done
}

url="http://www.npr.org/series/103063413/the-thistle-and-shamrock/archive?date="

# Data file for audiofiles, images, etc
queue="queue.data"
# Data file for missing elements
missing="missing.data"

archives=()

for archive in archives/*
do
	# Grab the archivelist div from the archive
	xml_grep --Html '//div[@class="archivelist"]' $archive > tidyArchive.tmp 2> /dev/null

	cp tidyArchive.tmp data-$archive
	
	# Parse out the episodes that have cover images
	items=$(xmlstarlet sel -T -t -v 'count(//article[@class="item has-image"])' tidyArchive.tmp)
	imageItems $archive $items

	# Rename itemsNoImages to something better.
	# Parse out the episodes that do not have cover images
	itemsNoImages=$(xmlstarlet sel -T -t -v 'count(//article[@class="item no-image"])' tidyArchive.tmp)
	noImageItems $archive $itemsNoImages
	
#	
#	echo "$archive contains $items element(s)"
#	for item in $(seq 1 $items)
#	do
#		printf "Parsing item %d :\t" $item
#
#		img=$(xmlstarlet sel -T -t -v "//div[@class='item-image'][${item}]/div/a/img/@src" tidyArchive.tmp)
#		img_title=$(xmlstarlet sel -T -t -v "//div[@class='item-image'][${item}]/div/a/img/@title" tidyArchive.tmp)
#		img_ref=$(xmlstarlet sel -T -t -v "//div[@class='item-image'][${item}]/div/a/@href" tidyArchive.tmp)
#		teaser=$(xmlstarlet sel -T -t -v "//div[@class='item-info'][${item}]/p/a/text()" tidyArchive.tmp)
#		teaser_ref=$(xmlstarlet sel -T -t -v "//div[@class='item-info'][${item}]/p/a/@href" tidyArchive.tmp)
#		data_audio=$(xmlstarlet sel -T -t -v "//div[@class='audio-module'][${item}]/div[@class='audio-module-controls-wrap']/@data-audio" tidyArchive.tmp)
#
#		if [ "$img_ref" == "$teaser_ref" ]; then
#			# ref's match, work it!
#
#
#			epi_date=$(echo $img_ref|cut -d'/' -f4,5,6 |tr / -)
#			#cuttitle=$(echo "$img_title" | sed 's/\.$//')
#			cuttitle=$(echo "$data_audio"|jq -r .title|sed -e 's/^.*: //g')
#			outfile="Thistle & Shamrock - $epi_date - $cuttitle"
#
#			srcfile=$(echo "$data_audio"|jq -r .audioUrl|base64 -d|cut -d'?' -f1)
#
#			printf "%s\n" "$cuttitle"
#
#			printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "$img_ref" "$img" "$srcfile" "$outfile" "$cuttitle" "$teaser" >> data-$archive.log
#		else
#			printf "ERROR parsing %s" "$img_title"
#		fi
#	done
done

